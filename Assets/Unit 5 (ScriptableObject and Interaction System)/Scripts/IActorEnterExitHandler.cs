using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IActorEnterExitHandler
{
    void ActorEnter(GameObject actor);
    void ActorExit(GameObject actor);
}
