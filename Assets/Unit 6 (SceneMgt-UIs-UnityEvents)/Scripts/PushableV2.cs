using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PushableV2 : MonoBehaviour
{
    [SerializeField] protected float m_Power = 10;
    private Rigidbody m_rigidBody;

    private void Start()
    {
        m_rigidBody = GetComponent <Rigidbody >();
    }

    public void Push(GameObject actor)
    {
        m_rigidBody.AddForce(actor.transform.forward*m_Power ,ForceMode.
            Impulse);
    }
}
