using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UIElements;

public class Timer : MonoBehaviour, IInteractable, IActorEnterExitHandler
{
    [SerializeField] protected float m_TimerDuration = 5;
    [SerializeField] protected float _CurrentTime;
    
    [SerializeField] protected UnityEvent m_TimerStartEvent = new();
    [SerializeField] protected UnityEvent m_TimerEndEvent = new();
    
    [SerializeField] protected TextMeshProUGUI m_TextInfoEToInteract;
    [SerializeField] protected float m_SliderTimer;
    //private float _SliderValue;

    [SerializeField] protected TextMeshProUGUI m_timer;
    
    protected bool _IsTimerStart = false;
    protected float _StartTimeStamp;
    protected float _EndTimeStamp;
    
    [SerializeField] protected float m_Power = 10;
    private Rigidbody m_rigidBody;

    public void ActorEnter(GameObject actor)
    {
        m_TextInfoEToInteract.gameObject.SetActive(true);
    }
    
    public void ActorExit(GameObject actor)
    {
        m_TextInfoEToInteract.gameObject.SetActive(false);
    }
    public void Interact(GameObject actor)
    {
        //Start the timer
        StartTimer();
    }
    
    // Start is called before the first frame update
    void Start()
    {
        m_rigidBody = GetComponent <Rigidbody >();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_IsTimerStart) return;
        
        _CurrentTime = (Time.time - _StartTimeStamp);
        m_timer.text = _CurrentTime.ToString("0.#");
        
        if (Time.time >= _EndTimeStamp)
        {
            EndTimer();
        }

        //_SliderValue = ((Time.time - _StartTimeStamp)/m_TimerDuration)*m_SliderTimer.maxValue;
        //m_SliderTimer.value = _SliderValue;

        if (Time.time >= _EndTimeStamp)
        {
            _IsTimerStart = false;
        }

        
    }

    public virtual void StartTimer()
    {
        //Check if the timer is already running
        if (_IsTimerStart) return;
        
        m_TimerStartEvent.Invoke();
        
        _IsTimerStart = true;
        _StartTimeStamp = Time.time;
        _EndTimeStamp = Time.time + m_TimerDuration;
        
        //_SliderValue = 0;
    }

    public virtual void EndTimer()
    {
        m_rigidBody.AddForce(Vector3.up*m_Power ,ForceMode.Impulse);
        m_TimerEndEvent.Invoke();
        _IsTimerStart = false;
    }
}
