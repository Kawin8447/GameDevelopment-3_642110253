using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabLauncher : MonoBehaviour
{
    [SerializeField] protected float m_Interval = 1.2f;
    [SerializeField] protected GameObject m_PrefabToBeLaunched;
    [SerializeField] protected float m_Power = 10;
    
    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(LaunchPrefab),0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void LaunchPrefab()
    {
        var g = Instantiate(m_PrefabToBeLaunched);
        g.transform.position = transform.position;
        var rb = g.GetComponent <Rigidbody >();
        rb.AddForce(transform.forward*m_Power ,ForceMode.Impulse);
        
        Destroy(g,3);
        
        Invoke(nameof(LaunchPrefab),m_Interval);
    }
}
